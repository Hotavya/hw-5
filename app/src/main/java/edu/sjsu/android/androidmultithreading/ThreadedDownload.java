package edu.sjsu.android.androidmultithreading;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ThreadedDownload extends AppCompatActivity {

    ImageView Image;
    ProgressDialog pg;
    String error_msg = "";
    private ThreadHandlerMainClass handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Image = findViewById(R.id.Image);
        pg = new ProgressDialog(ThreadedDownload.this);
        handler = new ThreadHandlerMainClass(Image);
    }


    public void runRunnable(View view) {
        try {
            final EditText etUrl = findViewById(R.id.etUrl);
            if (etUrl.getText().toString().length() == 0) {
                Toast.makeText(this, "Enter URL", Toast.LENGTH_SHORT).show();
                return;
            }

            pg.setCancelable(false);
            pg.setCanceledOnTouchOutside(false);
            pg.setTitle("Wait");
            pg.setMessage("Runnable Task");
            pg.show();

            new Thread(new Runnable() {
                public void run() {
                    final Bitmap m_image = downloadBitmap(etUrl.getText().toString());
                    handler.post(new Runnable() {
                        public void run() {
                            if (m_image != null) {
                                Image.setImageBitmap(m_image);
                            }
                            if (pg.isShowing()) {
                                pg.dismiss();
                            }
                        }
                    });
                }
            }).start();
        } catch (Exception e) {
            if (pg.isShowing()) {
                pg.dismiss();
            }
            Toast.makeText(this, "Error! " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /*Run Message Click Code */
    public void runMessage(View view) {
        try {

            pg.setCancelable(false);
            pg.setCanceledOnTouchOutside(false);
            pg.setTitle("Wait");
            pg.setMessage("Run Message Task");
            pg.show();


            final EditText etUrl = findViewById(R.id.etUrl);
            if (etUrl.getText().toString().length() == 0) {
                Toast.makeText(this, "Enter URL", Toast.LENGTH_SHORT).show();
                return;
            }

            new Thread(new Runnable() {
                public void run() {
                    Bitmap tmp = downloadBitmap(etUrl.getText().toString());

                    Message msg = handler.obtainMessage(0, tmp);
                    handler.sendMessage(msg);
                    if (pg.isShowing()) {
                        pg.dismiss();
                    }
                }
            }).start();

        } catch (Exception e) {
            if (pg.isShowing()) {
                pg.dismiss();
            }
            Toast.makeText(this, "Error! " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /*Run Async Task Button Click Code */
    public void runAsync(View view) {
        EditText etUrl = findViewById(R.id.etUrl);
        if (etUrl.getText().toString().length() == 0) {
            Toast.makeText(this, "Enter URL", Toast.LENGTH_SHORT).show();
            return;
        }
        pg.setCancelable(false);
        pg.setCanceledOnTouchOutside(false);
        pg.setTitle("Wait");
        pg.setMessage("Async Task");
        pg.show();
        AsyncTaskFromUrl asyncTaskFromUrl = new AsyncTaskFromUrl();
        asyncTaskFromUrl.execute("" + etUrl.getText().toString());

    }

    /*resetting the image  back to normal drawable Image */
    public void resetImage(View view) {
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
        Image.setImageBitmap(bitmap);
    }

    public Bitmap downloadBitmap(String urlSource) {

        try {
            URL url = new URL(urlSource);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (Exception e) {
            if (pg.isShowing()) {
                pg.dismiss();
            }

            // Log exception
            error_msg = "" + e.getMessage();

            // Toast.makeText(this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
            return null;
        }

    }


    /**
     * AsyncTAsk for Image Bitmap
     */
    public class AsyncTaskFromUrl extends AsyncTask<String, Void, Bitmap> {


        @Override
        protected Bitmap doInBackground(String... params) {

            System.out.println("Im in doInBackground");

            Bitmap bitmap = null;

            bitmap = downloadBitmap(params[0]);

            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {

            System.out.println("bitmap" + bitmap);
            if (bitmap == null) {
                Toast.makeText(ThreadedDownload.this, "Error! " + error_msg, Toast.LENGTH_SHORT).show();
                return;
            }
            Image.setImageBitmap(bitmap);
            if (pg.isShowing()) {
                pg.dismiss();
            }

        }
    }


    class ThreadHandlerMainClass extends Handler {
        private ImageView tmpImgView;

        public ThreadHandlerMainClass(ImageView imgv) {
            tmpImgView = imgv;
        }

        public void handleMessage(Message msg) {
            if ((Bitmap) msg.obj != null) {
                tmpImgView.setImageBitmap((Bitmap) msg.obj);

            } else {
                Toast.makeText(ThreadedDownload.this, "Error!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}